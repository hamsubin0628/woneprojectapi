package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Category;
import com.wone.woneprojectapi.model.category.CategoryCreateRequest;
import com.wone.woneprojectapi.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public Category getData(long id){ return categoryRepository.findById(id).orElseThrow(); }

    public void setCategory(CategoryCreateRequest request){
        Category addData = new Category();
        addData.setCategoryType(request.getCategoryType());

        categoryRepository.save(addData);
    }
}
