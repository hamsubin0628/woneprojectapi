package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Card;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.card.CardCreateRequest;
import com.wone.woneprojectapi.model.card.CardInfoChangeRequest;
import com.wone.woneprojectapi.model.card.CardResponse;
import com.wone.woneprojectapi.repository.CardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardService {
    private final CardRepository cardRepository;

    public void setCard(Member member, CardCreateRequest request){
        Card addData = new Card();
        addData.setMember(member);
        addData.setCardGroup(request.getCardGroup());
        addData.setCardNumber(request.getCardNumber());
        addData.setEndDate(request.getEndDate());
        addData.setCvc(request.getCvc());
        addData.setEtcMemo(request.getEtcMemo());

        cardRepository.save(addData);
    }

    public CardResponse getCard(long id){
        Card originData = cardRepository.findById(id).orElseThrow();
        CardResponse response = new CardResponse();
        response.setId(originData.getId());
        response.setMemberId(originData.getMember().getId());
        response.setCardGroupName(originData.getCardGroup().getCardCompany());
        response.setCardNumber(originData.getCardNumber());
        response.setEndDate(originData.getEndDate());
        response.setCvc(originData.getCvc());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putCard(long id, CardInfoChangeRequest request){
        Card originData = cardRepository.findById(id).orElseThrow();
        originData.setCardGroup(request.getCardGroup());
        originData.setCardNumber(request.getCardNumber());
        originData.setEndDate(request.getEndDate());
        originData.setCvc(request.getCvc());
        originData.setEtcMemo(request.getEtcMemo());

        cardRepository.save(originData);
    }
}
