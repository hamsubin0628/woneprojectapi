package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Calendar;
import com.wone.woneprojectapi.entity.Category;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.calendar.CalendarCreateRequest;
import com.wone.woneprojectapi.model.calendar.CalendarResponse;
import com.wone.woneprojectapi.model.calendar.CalendarSpendingChangeRequest;
import com.wone.woneprojectapi.repository.CalendarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CalendarService {
    private final CalendarRepository calendarRepository;

    public void setCalendar(Member member, Category category, CalendarCreateRequest request){
        Calendar addDate = new Calendar();
        addDate.setMember(member);
        addDate.setMoneyType(request.getMoneyType());
        addDate.setSpendDate(LocalDateTime.now());
        addDate.setCategory(category);
        addDate.setAmount(request.getAmount());

        calendarRepository.save(addDate);
    }

    public CalendarResponse getCalendar(long memberId){
        Calendar originData = calendarRepository.findById(memberId).orElseThrow();
        CalendarResponse response = new CalendarResponse();
        response.setMemberId(originData.getMember().getId());
        response.setMoneyTypeName(originData.getMoneyType().getMoneyType());
        response.setSpendDate(originData.getSpendDate());
        response.setCategoryId(originData.getCategory().getId());
        response.setAmount(originData.getAmount());

        return response;
    }

    public void putSpending(long id, CalendarSpendingChangeRequest request){
        Calendar originData = calendarRepository.findById(id).orElseThrow();
        originData.setAmount(request.getAmount());
        calendarRepository.save(originData);
    }

    public void delSpending(long id){
        calendarRepository.deleteById(id);
    }
}
