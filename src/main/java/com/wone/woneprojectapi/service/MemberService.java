package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.member.MemberDupCheckResponse;
import com.wone.woneprojectapi.model.member.MemberJoinRequest;
import com.wone.woneprojectapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){ return memberRepository.findById(id).orElseThrow(); }

    public MemberDupCheckResponse getMemberIdDupCheck(String username) {
        MemberDupCheckResponse response = new MemberDupCheckResponse();
        response.setIsNew(isNewUsername(username));

        return response;
    }

    public void setMember(MemberJoinRequest request){
        Member addData = new Member();

        addData.setUserId(request.getUserId());
        addData.setUsername(request.getUsername());
        addData.setBirthDate(request.getBirthDate());
        addData.setEMail(request.getEMail());
        addData.setPassword(request.getPassword());
        addData.setIsReceive(request.getIsReceive());
        addData.setJoinDate(request.getJoinDate());
        addData.setMemberGroup(request.getMemberGroup());
        addData.setMemberStatus(request.getMemberStatus());

        memberRepository.save(addData);
    }

    /**
     * 회원을 가입 시킨다.
     *
     * @param request 회원가입창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복일 경우, 비밀번호와 비밀번호 확인이 일치하지 않을 경우
     */
    public void setMemberJoin(MemberJoinRequest request) throws Exception {
        if (!isNewUsername(request.getUsername())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception(); // ==은 숫자, ==은 값만 비교함
    }

    /**
     * 신규 아이디 인지 확인한다.
     * @param username 아이디
     * @return true 신규아이디 / false 중복아이디
     */
    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);

        return dupCount <= 0;
    }
}
