package com.wone.woneprojectapi.service;

import com.wone.woneprojectapi.entity.Calendar;
import com.wone.woneprojectapi.entity.Challenge;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.challenge.*;
import com.wone.woneprojectapi.repository.CalendarRepository;
import com.wone.woneprojectapi.repository.ChallengeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ChallengeService {
    private final ChallengeRepository challengeRepository;


    public void setChallenge(Member member, ChallengeCreateRequest request){
        Challenge addData = new Challenge();
        addData.setMember(member);
        addData.setChallengeDate(LocalDate.now());
        addData.setGoalsMoney(request.getGoalsMoney());

        challengeRepository.save(addData);
    }

    public List<ChallengeItem> getChallenges(){
        List<Challenge> originData = challengeRepository.findAll();
        List<ChallengeItem> result = new LinkedList<>();

        for (Challenge challenge : originData){
            ChallengeItem addItem = new ChallengeItem();
            addItem.setId(challenge.getId());
            addItem.setMemberId(challenge.getMember().getId());
            addItem.setChallengeDate(challenge.getChallengeDate());
            addItem.setGoalsMoney(challenge.getGoalsMoney());

            result.add(addItem);
        }
        return result;
    }

    public ChallengeResponse getChallenge(long id){
        Challenge originData = challengeRepository.findById(id).orElseThrow();
        ChallengeResponse response = new ChallengeResponse();
        response.setId(originData.getId());
        response.setMemberId(originData.getMember().getId());
        response.setChallengeDate(originData.getChallengeDate());
        response.setGoalsMoney(originData.getGoalsMoney());

        return response;
    }

    public void putChallenge(long id, ChallengeGoalsMoneyChangeRequest request){
        Challenge originData = challengeRepository.findById(id).orElseThrow();
        originData.setGoalsMoney(request.getGoalsMoney());

        challengeRepository.save(originData);
    }

}
