package com.wone.woneprojectapi.model.calendar;

import com.wone.woneprojectapi.enums.MoneyType;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;

import java.time.LocalDateTime;

@Getter
@Setter
public class CalendarResponse {
    private Long memberId;
    private String  moneyTypeName;
    private LocalDateTime spendDate;
    private Long categoryId;
    private Double amount;
}
