package com.wone.woneprojectapi.model.calendar;

import com.wone.woneprojectapi.entity.Category;
import com.wone.woneprojectapi.enums.MoneyType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CalendarCreateRequest {
    private Long memberId;

    @Enumerated(value = EnumType.STRING)
    private MoneyType moneyType;

    private LocalDateTime spendDate;

    private Long categoryId;

    private Double amount;
}
