package com.wone.woneprojectapi.model.card;

import com.wone.woneprojectapi.enums.CardGroup;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CardInfoChangeRequest {
    private CardGroup cardGroup;
    private String cardNumber;
    private LocalDate endDate;
    private Short cvc;
    private String etcMemo;
}
