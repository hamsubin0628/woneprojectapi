package com.wone.woneprojectapi.model.member;

import com.wone.woneprojectapi.enums.MemberGroup;
import com.wone.woneprojectapi.enums.MemberStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {
    private String userId;

    private String username;

    private LocalDate birthDate;

    private String eMail;

    private String password;

    private String passwordRe;

    private Boolean isReceive;

    private LocalDate joinDate;

    @Enumerated(value = EnumType.STRING)
    private MemberGroup memberGroup;

    private MemberStatus memberStatus;
}
