package com.wone.woneprojectapi.model.category;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryCreateRequest {
    private String categoryType;
}
