package com.wone.woneprojectapi.model.challenge;

import com.wone.woneprojectapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ChallengeItem {
    private Long id;
    private Long memberId;
    private LocalDate challengeDate;
    private Double goalsMoney;
}
