package com.wone.woneprojectapi.model.challenge;

import com.wone.woneprojectapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RemainingResponse {
    private Long memberId;
    private Double goalsMoney;
    private Double totalAmount;
    private Double totalRemaining;
}
