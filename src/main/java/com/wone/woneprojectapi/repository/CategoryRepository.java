package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
