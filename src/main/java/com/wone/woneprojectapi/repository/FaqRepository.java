package com.wone.woneprojectapi.repository;

import com.wone.woneprojectapi.entity.Faq;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FaqRepository extends JpaRepository <Faq, Long>{
}
