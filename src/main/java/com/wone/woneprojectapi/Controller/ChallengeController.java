package com.wone.woneprojectapi.Controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.challenge.ChallengeCreateRequest;
import com.wone.woneprojectapi.model.challenge.ChallengeGoalsMoneyChangeRequest;
import com.wone.woneprojectapi.model.challenge.ChallengeItem;
import com.wone.woneprojectapi.model.challenge.ChallengeResponse;
import com.wone.woneprojectapi.service.ChallengeService;
import com.wone.woneprojectapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/challenge")
public class ChallengeController {
    private final MemberService memberService;
    private final ChallengeService challengeService;

    @PostMapping("/member-id/{memberId}")
    public String setChallenge(@PathVariable long memberId, @RequestBody ChallengeCreateRequest request){
        Member member = memberService.getData(memberId);
        challengeService.setChallenge(member ,request);

        return "OK";
    }

    @GetMapping("/all")
    public List<ChallengeItem> getChallenges(){
        return challengeService.getChallenges();
    }

    @GetMapping("/challenge-detail/{id}")
    public ChallengeResponse getChallenge(@PathVariable long id){
        return challengeService.getChallenge(id);
    }

    @PutMapping("/challenge-goals-money/{id}")
    public String putChallenge(@PathVariable long id, @RequestBody ChallengeGoalsMoneyChangeRequest request){
        challengeService.putChallenge(id, request);

        return "OK";
    }
}
