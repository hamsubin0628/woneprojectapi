package com.wone.woneprojectapi.Controller;

import com.wone.woneprojectapi.model.member.MemberDupCheckResponse;
import com.wone.woneprojectapi.model.member.MemberJoinRequest;
import com.wone.woneprojectapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberJoinRequest request) throws Exception {
        memberService.setMemberJoin(request);
        // 얘도 던지면 setMember 도 던짐

        return "OK";
    }

    @GetMapping("/check/id") //있는지 확인하는 것이므로 GetMapping, 여기에 선언하면 필수 값을 선언하는 것
    public MemberDupCheckResponse getMemberIdCheck(
            @RequestParam(name = "username") String username,
            @RequestParam(name = "goodsName", required = false, defaultValue = "몰라") String goodsName,
            @RequestParam(name = "minPrice", required = false, defaultValue = "0") double minPrice
            // 파라미터의 문제점 : 기본적으로 String이기 때문에 검사해야함

    ) {
        return memberService.getMemberIdDupCheck(username);
    }
}
