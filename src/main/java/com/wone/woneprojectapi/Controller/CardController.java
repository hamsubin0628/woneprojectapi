package com.wone.woneprojectapi.Controller;

import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.card.CardCreateRequest;
import com.wone.woneprojectapi.model.card.CardInfoChangeRequest;
import com.wone.woneprojectapi.model.card.CardResponse;
import com.wone.woneprojectapi.service.CardService;
import com.wone.woneprojectapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/card")
public class CardController {
    private final MemberService memberService;
    private final CardService cardService;

    @PostMapping("/member-id/{memberId}")
    public String setCard(@PathVariable long memberId, @RequestBody CardCreateRequest request){
        Member member = memberService.getData(memberId);
        cardService.setCard(member, request);

        return "OK";

    }

    @GetMapping("/card-detail/{id}")
    public CardResponse getCard(@PathVariable long id){
        return cardService.getCard(id);
    }

    @PutMapping("/card-info/{id}")
    public String putCard(@PathVariable long id, @RequestBody CardInfoChangeRequest request){
        cardService.putCard(id, request);

        return "OK";
    }
}
