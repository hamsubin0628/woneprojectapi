package com.wone.woneprojectapi.Controller;

import com.wone.woneprojectapi.entity.Category;
import com.wone.woneprojectapi.entity.Member;
import com.wone.woneprojectapi.model.calendar.CalendarCreateRequest;
import com.wone.woneprojectapi.model.calendar.CalendarResponse;
import com.wone.woneprojectapi.model.calendar.CalendarSpendingChangeRequest;
import com.wone.woneprojectapi.service.CalendarService;
import com.wone.woneprojectapi.service.CategoryService;
import com.wone.woneprojectapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/calendar")
public class CalendarController {
    private final MemberService memberService;
    private final CategoryService categoryService;
    private final CalendarService calendarService;

    @PostMapping("/member-id/category-id/{memberId}/{categoryId}")
    public String setCalendar(@PathVariable long memberId, @PathVariable long categoryId ,@RequestBody CalendarCreateRequest request){
        Member member = memberService.getData(memberId);
        Category category = categoryService.getData(categoryId);
        calendarService.setCalendar(member, category ,request);

        return "OK";
    }

    @GetMapping("/one-day-spending/{memberId}")
    public CalendarResponse getCalender(@PathVariable long memberId){
        return calendarService.getCalendar(memberId);
    }

    @PutMapping("/spending-change/{id}")
    public String putSpending(@PathVariable long id, CalendarSpendingChangeRequest request){
        calendarService.putSpending(id, request);

        return "OK";
    }

    @DeleteMapping("/spending/{id}")
    public String delSpending(@PathVariable long id){
        calendarService.delSpending(id);

        return "OK";
    }
}
