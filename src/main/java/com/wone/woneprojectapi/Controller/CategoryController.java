package com.wone.woneprojectapi.Controller;

import com.wone.woneprojectapi.model.category.CategoryCreateRequest;
import com.wone.woneprojectapi.service.CategoryService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/category")
public class CategoryController {
    private final CategoryService categoryService;

    @PostMapping("/new")
    public String setCategory(@RequestBody CategoryCreateRequest request){
        categoryService.setCategory(request);

        return "OK";
    }
}
