package com.wone.woneprojectapi.entity;

import com.wone.woneprojectapi.enums.MemberGroup;
import com.wone.woneprojectapi.enums.MemberStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15, unique = true)
    private String userId;

    @Column(nullable = false, length = 20)
    private String username;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Column(nullable = false, length = 50)
    private String eMail;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String passwordRe;

    @Column(nullable = false)
    private Boolean isReceive;

    @Column(nullable = false)
    private LocalDate joinDate;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private MemberGroup memberGroup;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private MemberStatus memberStatus;

    private LocalDate outDate;
}
